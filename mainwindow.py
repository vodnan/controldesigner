# This Python file uses the following encoding: utf-8
import sys

#from PySide6.QtWidgets import QApplication, QMainWindow

from scipy.integrate import odeint
from scipy.integrate import solve_ivp
from scipy import signal

from scitools.StringFunction import StringFunction

from sympy.parsing.sympy_parser import parse_expr
from sympy.solvers.ode import dsolve

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QAction
from PyQt6.QtWidgets import QApplication, QLabel, QMainWindow, QMenu, QLineEdit, QVBoxLayout, QHBoxLayout, QGridLayout, QWidget, QPushButton, QComboBox, QToolBar

import pyqtgraph as pg

from ControlDesign import ControlDesign

# Important:
# You need to run the following command to generate the ui_form.py file
#     pyside6-uic form.ui -o ui_form.py, or
#     pyside2-uic form.ui -o ui_form.py
from ui_form import Ui_MainWindow

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        ui = Ui_MainWindow()
        ui.setupUi(self)
        ui.pushButton.pressed.connect(self.plot)
        self.control_designer = ControlDesign(ui)
        #ui.controlDesignStyle.currentIndexChanged.connect(self.)
    def plot(self):
        self.control_designer.plot()
        """
        self.ui.plotWidget.clear()
        x = np.arange(1000)
        y = np.random.normal(size=(3, 1000))
        for i in range(3):
           self.plotted = self.ui.plotWidget.plot(x, y[i], pen=(i,3))
        """




if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = MainWindow()
    widget.show()
    sys.exit(app.exec())
