# This Python file uses the following encoding: utf-8

import numpy as np

class SystemModel:
    def __init__(self):
        self.model_ss_A = None
        self.model_ss_B = None
        self.model_ss_C = None
        self.model_ss_D = None

        self.model_tf_siso = None

class ControlDesign:
    def __init__(self, window):
        self.window = window
        self.model = SystemModel()


    def plot(self):
        self.get_model_from_ui()
        self.window.plotWidget.clear()
        x = np.arange(1000)
        y = np.random.normal(size=(3, 1000))
        for i in range(3):
           self.plotted = self.window.plotWidget.plot(x, y[i], pen=(i,3))


    def get_model_from_ui(self):
        current_design_style = self.window.controlDesignStyle.currentIndex()
        if current_design_style == 0: # TF design
            pass
        elif current_design_style == 1: # SS design
            pass
